require 'logger'
require 'active_support/all'


module KlassLogger
  attr_reader :log
  def initialize
    @log = Logger.new(STDOUT)
  end

  def hello
    log.info('in')
    super
    log.info('out')
  end
end

class Klass
  # ruby 2.0 and above
  prepend KlassLogger

  def hello
    "hello"
  end
end

puts Klass.new.hello
