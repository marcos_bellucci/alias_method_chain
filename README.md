
# Understanding alias_method_chain and prepend 

#### Tags: learning ruby, rails

##### No words, just code
> In order to understand how alias_method_chain works i look forward you to read each of the file versions in order by:

```bash
git show HEAD~5:program.rb
git show HEAD~4:program.rb
git show HEAD~3:program.rb
git show HEAD~2:program.rb
git show HEAD~1:program.rb
git show HEAD:program.rb
```
